import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Network } from '@capacitor/network'

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  constructor(public toastController: ToastController) {}

  async ngOnInit() {
    await this.getNetworkStatus();
  }

  async getNetworkStatus() {
    const network = (await Network.getStatus()).connectionType;
    
    const toast = await this.toastController.create({
      message: 'Type de connexion : '+network,
      duration: 2000
    });
    toast.present();
  }

}
