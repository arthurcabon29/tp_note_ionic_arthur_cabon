import { Component, OnInit } from '@angular/core';
import { Geolocation } from '@capacitor/geolocation';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {

  constructor() {}

  public tabLocalistion = [];


  async ngOnInit() {
    await this.printCurrentPosition();
  }

  async printCurrentPosition() {
    const coordinates = await Geolocation.watchPosition({}, (position, err) => {
      if(err === undefined) {
        this.tabLocalistion.push(position);
        if (this.tabLocalistion.length > 5) {
          this.tabLocalistion.shift()
        }
        console.log(this.tabLocalistion);
      } else {
        console.log(err);
      }
    });
  
    // console.log('Current position:', coordinates);
  };

}
